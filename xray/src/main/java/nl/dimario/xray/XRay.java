package nl.dimario.xray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

public class XRay {

  private static final int MAXLINELENGTH=132;
  
  /**
   * Create a list of all key/value pairs that are the properties as obtained from
   * System.getProperties(). For some keys or values, we process the value before adding it to the
   * result: 
   * value == null becomes value = "(null)" 
   * value == "" becomes value = "(empty String)" 
   * key == "line.separator" becomes value is transformed to hexadecimal
   * key contains the word "path": the value is split up using 
   * path.separator char and newlines (order is maintained).
   * 
   * Also, values exceeding MAXLINELENGTH will be cut up by interspersing newlines.
   * 
   * Before returning, the list is sorted on order of the keys.
   */
  public static List<KeyValue> getSystemInfo() {

    // This is the returned argument
    List<KeyValue> list = new ArrayList<KeyValue>();

    // Here are all the properties that we're interested in
    Properties properties = System.getProperties();

    // Process each property separately.
    Enumeration<?> names = properties.propertyNames();
    while (names.hasMoreElements()) {
      String key = (String) names.nextElement();
      String value = properties.getProperty(key);
      if (value == null) {
        value = "(null)";
      } else if (value.length() < 1) {
        value = "(empty String)";
      } else if ("line.separator".equalsIgnoreCase(key)) {
        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < value.length(); i++) {
          sb.append(String.format("%02X", (int) value.charAt(i)));
          sb.append(" ");
        }
        value = sb.toString();
        key = key + " (hex)";
      } else if (key.contains("path")) {
        value = prettyPath(value);
      } else {
        value = hackitup( value);
      }
      
      // Add the key and processed value to the output list
      KeyValue kv = new KeyValue(key, value);
      list.add(kv);
    }
    // Sort output on key value
    Collections.sort(list, KeyValue.BYKEY);
    return list;
  }

  /**
   * Split elements of a path (for example a classpath) in such a way that they are separated by
   * newlines and each entry is printed by the JSP starting at a new line. The order of the entries
   * is maintained.
   */
  private static String prettyPath(String value) {
    String separator = System.getProperty("path.separator");
    if (value.equals(separator)) {
      // The value has no elements, only a separator char.
      return value;
    }

    // split the value on the separator char
    String[] paths = value.split(separator);
    
    // Add the separate entries to the output but 
    // separate them with a newline. 
    StringBuilder sb = new StringBuilder();

    boolean newLine = false;
    for (String path : paths) {
      if (newLine) {
        sb.append("\n");
      } else {
        newLine = true;
      }
      sb.append(path);
    }
    return sb.toString();
  }
  
  /**
   * This method takes a String of arbitrary length and intersperses it with newline characters
   * so that when the string is printed it will not be excessively wide.
   * The cutoff colum is given by MAXLINELENGTH. Any input shorter than this length will 
   * be returned unchanged. Currently this method ignores the fact that the input String
   * may already contain newlines. Arguably, this results in crude formatting of the output
   * and for the purpose at hand this is deemed good enough.
   */
  private static String hackitup( String value) {
    
    // Lines that are short enough to start with are left unchanged.
    if( value.length() <= MAXLINELENGTH) {
      return value;
    }
    
    StringBuilder sb= new StringBuilder();
    
    // chunks is the number of substrings of MAXLINELENGTH that can be cut out of the input.
    // Integer division drops the remainder, we'll take care of that after the loop.
    int chunks = value.length() / MAXLINELENGTH;
    for( int i=0; i<chunks; i++) {
      // Take the i'th substring, addit to the output buffer then add a trailing newline.
      String sub = value.substring(i * MAXLINELENGTH, (i+1) * MAXLINELENGTH);
      sb.append( sub);
      sb.append( "\n");
    }
    // Add the remainder of the input without an extra trailing newline. 
    sb.append(value.substring(chunks * MAXLINELENGTH));
    
    return sb.toString();
  }
}
