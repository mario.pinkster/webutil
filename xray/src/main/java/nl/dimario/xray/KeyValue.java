package nl.dimario.xray;

/**
 * A simple key/value pair with a comarator for sorting ion key.
 */
import java.util.Comparator;

public class KeyValue {

  private String key;
  private String value;

  public KeyValue(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return this.key;
  }

  public String getValue() {
    return this.value;
  }

  public static Comparator<KeyValue> BYKEY = new Comparator<KeyValue>() {

    @Override
    public int compare(KeyValue o1, KeyValue o2) {

      return o1.getKey().compareTo(o2.getKey());
    }
  };
}
