# webutil
This is a collection of unrelated utilities that you may find handy to incorporate 
into your webserver project.

* logread finds the application logfile and formats the tail end into a String 
intended for display in a web page. Currently this works only for logfiles written to via logback.

* xray lists all system properties from System.getProperties() in a List<KeyValue>
 with some embellishments. KeyValue is a simple pair of Strings.

* testsite is a simple website that has servlets demonstrating how to use these 
utilies and display the results in a JSP. In addition it has a servlet demonstrating
how to render markdown as HTML.

The pom.xml in the root is the  parent of all three modules.
