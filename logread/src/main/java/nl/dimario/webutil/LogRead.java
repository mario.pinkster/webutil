package nl.dimario.webutil;

/**
 * These static methods try to read the tail end of a log file
 * and display a number of lines from it.
 * Which logfile is used (if any) is determined by sifting through all
 * log appenders that have been defined in the logging configuration
 * and picking the first likely candidate that appears to be a file.
 * The file is then opened for random access and a number of chars is read from
 * the tail end of it. An attempt is made to roughly get a predetermined
 * number of line on the assumption that an average line is 80 characters.
 * Finally, the tail end of the log file is returned in the response. 
 */
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Iterator;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.FileAppender;

public class LogRead  {

  /**
   * Find a logfile from the information that we can get from the logback logging library.
   * 
   * @return a logfile or null if no logfile could be found.
   */
  public static File getLogFile() {

    File clientLogFile;
    FileAppender<?> fileAppender = null;
    
    // Walk the Logger list that we obtain from the logger context
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    for (Logger logger : context.getLoggerList()) {

      // Inner loop: walk the Appenders for this Logger
      for (Iterator<Appender<ILoggingEvent>> index = logger.iteratorForAppenders(); index
          .hasNext();) {
        Object enumElement = index.next();
        
        // If this Appender is a FileAppender we will use this one.
        if (enumElement instanceof FileAppender) {
          fileAppender = (FileAppender<?>) enumElement;
          break;
        }
      }
      if( fileAppender != null) {
        break;
      }
    }
    
    // If found, use the FileAppender to create the returned File.
    if (fileAppender != null) {
      clientLogFile = new File(fileAppender.getFile());
    } else {
      clientLogFile = null;
    }
    return clientLogFile;
  }
  
  /**
   * Read the tail end of the log file using random access.
   * 
   * @param logFile  The file we must read from
   * @param chars the number of characters to read from the end of the file.
   * 
   * @return The trailing end of the file as a String (or the whole file
   * if it is smaller than that). The string is formatted with HTML line breaks.
   * 
   * Note: we position the start of the returned string exactly at
   * the beginning of a log line and for this reason return a smaller
   * number of characters than was actually requested.
   */
  public static String readLast( File logFile, int chars) {
    
    StringBuilder sb = new StringBuilder();
    
    try (RandomAccessFile raf = new RandomAccessFile( logFile, "r")) {
      
      // Caclulate seek position as an offset from the end of the file
      long startHere = logFile.length() - chars;
      if( startHere < 0) {
        // The file is shorter than the number of chars we want to read,
        // set seek position to start of file.
        startHere = 0;
      }
      
      // Go to the calculated seek position
      raf.seek( startHere);
      
      // Read the first (most likely partial) log line and discard it.
      String line = raf.readLine();
      
      // Now continue to add lines to the output until EOF.
      // If in the meanwhile new lines are appended to the logfile 
      // they may or may not be included; I haven't investigated this thoroughly.
      while( (line = raf.readLine()) != null ) {
        // Format it nicely for HTML
        sb.append( line);
        sb.append( "<br>");
      }
          
    } catch( Exception x) {
    
      // Format the stack trace nicely for HTML
      sb.append( x.getMessage());
      for( StackTraceElement ste : x.getStackTrace()) {
        sb.append( "<br>");
        sb.append( ste.toString());
      }
    }
    
    return sb.toString();
  }
}
