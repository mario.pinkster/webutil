package nl.dimario.web;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.dimario.xray.KeyValue;
import nl.dimario.xray.XRay;

@WebServlet(urlPatterns = "/xray", name = "XRayServlet")
public class XrayServlet extends HttpServlet {


  private static Logger LOG = LoggerFactory.getLogger(XrayServlet.class);

  private static final long serialVersionUID = 1317L;

  private static final String SYSTEMINFO = "systemInfo";
  private static final String XRAY_JSP = "xray.jsp";

  private static AtomicInteger count = new AtomicInteger(1);

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {

    LOG.info("You have called XRay {} times", count.getAndIncrement());

    List<KeyValue> keyValues = XRay.getSystemInfo();
    request.setAttribute(SYSTEMINFO, keyValues);

    RequestDispatcher rd = request.getRequestDispatcher(XRAY_JSP);
    rd.forward(request, response);
  }
}
