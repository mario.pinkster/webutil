package nl.dimario.web;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

@WebServlet(urlPatterns = "/markdown", name = "MarkdownServlet", displayName = "Serve markdown as HTML")
public class MarkdownServlet extends HttpServlet {
  
  private static final long serialVersionUID = 1337L;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    
    // Read file as String
    File file = new File( "src/main/webapp/markdown.md");
    String markdownInput = FileUtils.readFileToString(file,"UTF-8");
    
    Parser markdownParser = Parser.builder().build();
    Node document = markdownParser.parse( markdownInput);
    HtmlRenderer renderer = HtmlRenderer.builder().build();
    String htmlOutput = renderer.render(document);
    
    response.setContentType( "text/html");
    Writer writer = response.getWriter();
    writer.write( "<html><head></head><body>");
    writer.write( htmlOutput);
    writer.write( "</body></html>");
    writer.flush();
  }
}
