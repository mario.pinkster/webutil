package nl.dimario.web;

/**
 * This servlet uses the LogRead utility project to display the tail
 * end of the application log file.
 */
import java.io.File;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.dimario.webutil.LogRead;

@WebServlet(urlPatterns = "/logview", name = "logViewServlet", displayName = "Log View Servlet")
public class LogViewServlet extends HttpServlet {

  private static final long serialVersionUID = 13737L;
  
  private final static int    CHARS_PER_LINE = 132;
  private final static int    DEFAULT_LINES = 24;
  private final static String LINES = "lines";
  private final static String LOGCONTENT = "logcontent";
  private final static String LOGNAME = "logname";
  private final static String LOGVIEW_JSP = "logview.jsp";

  public LogViewServlet() {
    super();
  }

  /**
   * This servlet only implements the HTTP GET request.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // set char encoding to sensible default
    request.setCharacterEncoding("UTF-8");

    // determine the number of lines we must get from the tail end of the log.
    int lines = DEFAULT_LINES;
    String slines = request.getParameter( LINES);
    try {
      lines = Integer.parseInt(slines);
    } catch( Exception x) {
      lines = DEFAULT_LINES;
    }
    int chars = lines * CHARS_PER_LINE;
    
    File logFile = LogRead.getLogFile();
    String logContent = "";
    String logname = "Unable to deternime name of logfile";
    if(logFile != null) {
      logname = logFile.getPath();
      logContent = LogRead.readLast( logFile, chars);
    }
    request.setAttribute(LOGCONTENT,logContent);
    request.setAttribute(LOGNAME, logname);
    RequestDispatcher rd = request.getRequestDispatcher(LOGVIEW_JSP);
    rd.forward(request, response);
  }
}
