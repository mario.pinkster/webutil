package nl.dimario.web;

/**
 * This servlet is used to add lines to the application log
 * file so that we see things moving in the logview demo.
 * 
 * Every GET request directed here generates a log message
 * with an incrementing counter.
 */
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet(urlPatterns = "/click", name = "clickServlet")
public class ClickServlet extends HttpServlet {
  
  private static Logger LOG = LoggerFactory.getLogger(ClickServlet.class);

  private static final String MSG = "msg";

  private static final long serialVersionUID = 731L;
  
  private static AtomicInteger count = new AtomicInteger( 1);

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    
    String msg = request.getParameter(MSG);
    if( msg == null) {
      msg = "";
    }
    
    LOG.info( "You have clicked {} times: {}", count.getAndIncrement(), msg);
    
  }
}
