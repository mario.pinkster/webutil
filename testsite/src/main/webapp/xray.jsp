<%@ page language="java" isELIgnored="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
  pageContext.setAttribute("systeminfo", request.getAttribute("systeminfo"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>X-Ray</title>

<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 900px;
}

th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

td {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
	white-space: pre;
}

tr:nth-child(even) {
	background-color: #dddddd;
}
</style>

</head>
<body>

	<table>
		<caption>
			Information from <b>System.getProperties()</b>
		</caption>
		<thead>
			<tr>
				<th class="name"  scope="col">Name</th>
				<th class="value" scope="col">Value</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${ systemInfo }">
				<tr>
					<td><c:out value="${ item.key }" /></td>
					<td><c:out value="${ item.value }" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>