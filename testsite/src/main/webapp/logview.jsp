<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  pageContext.setAttribute("logcontent", request.getAttribute("logcontent"));
  pageContext.setAttribute("logname", request.getAttribute("logname"));
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Application Log</title>

</head>
<body>
	<H1>Application log tail</H1>
	
	<p><a href="index.html">back to index</a></p>

	<p>&nbsp;</p>
	Log name: ${logname}
	<p>&nbsp;</p>
	<p>${logcontent}</p>

</body>
</html>