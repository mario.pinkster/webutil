## Running this application in a server
Here are some directions on getting this beast running as a web application in Eclipse, 
both for Apache Tomcat and for Jetty.

### Apache Tomcat

I installed Apache Tomcat 9 in Eclipse with all default settings. Installation is straight forward and there are
numerous howtos to be found on the Internet explaining how to do this. 

After building all projects with Maven from the command line (start in the parent project, this will
automatically build the child projects too) you need to tweak some things in your Eclipse
settings for the **testsite** project in order to get it running as a webapp in Apache Tomcat. 

#### 1 Add project facets
Open the project context menu for the testsite project and select *properties* (at the bottom).
In that dialogue, first click *Project facets*.

Eclipse will see that your project is not a faceted project yet and provide a link action
to turn it into one. Click the blue text that reads *Convert to faceted form...*

You then get the *Project Facets* dialog  where you must check *Dynamic Web Module*. You can leave the
version at the default value (3.0 in my version of Eclipse).

Save this change.

**Note:** if you forget this step your project will not show up in the Apache Tomcat *Add and remove*
dialog because Eclipse won't recognize it as a web application.

#### 2 Change the *Web Project Settings*
Again, open the project properties dialog and select *Web Project Settings*
(these show up after you reopen the properties for your newly faceted project).
The value of *Context root* is **testsite** because that is the name of the project.
**Change this to "test"** because that is the name I use in index.html.

Again, save this change (confirm the change).

#### 3 Change the deployment assembly.
Again, open *Project->Properties* for the **testsite** project and select *Deployment Assembly*. 
This brings up a dialog which shows which project directories are used in what way when creating 
the *.war file.

There is an entry in the list for the **WebContent** directory. **Remove this entry**
(and also remove the directory from your file system. This is an nonsense
directory that is created when Eclipse converts your  project to a faceted version).

Instead, your **web root "/"** entry should be linked to **src/main/webapp** which is the
correct Maven default for web applications. Add this folder to the list, Eclipse
will automatically make it the new web root.

You must now also add an entry for the Maven dependencies. Again, select the *Add* button
in this dialog and then tell Eclipse you want to add *Java Build Path Entries*.
In the next step, select *Maven Dependencies* and save all changes.

At this point Eclipse may or may not start complaining about 
````
Invalid classpath publish/export dependency /logread. Project entries not supported.
````
Actually, we don't want to add it to our exports and publish it.

This is just another instance of Eclipse trying to do something that we don't want
it to do (and complaining when it fails). I got rid of this error by adding the
*logread* project to the deployment assembly. This has the added advantage that
when running in debug mode Eclipse can find the sources for that project.

Another way of getting rid of this error message is to look in *Project->Properties->Maven*
and uncheck *Resolve dependencies from Workspace projects*. This in turn causes another error,
that tells you there is a mismatch in Java compiler version declarations. 

Inspect both versions and make them equal by changing one or the other:

* The Maven **parent** pom for testutil has the desired Java version here:
````
	<properties>
		...
		<java.version>1.8</java.version>
		...
	</properties>
````

* The Project Facet version can be changed by going to the project facets dialogue: 

*Project->Properties->Project Facets*

Once there you see various facets listed together with their current version number. 
All listed version numbers are dropdown elements showing the currently selected version.
By clicking the text of the version a selection list appears where you can change the current value.

For good measure, after making both Java versions equal to each other, 
rebuild all the projects from scratch and refresh the projects in Eclipse.

Now add the *testsite* project to the Apache Tomcat server by either using *Run As->Run On Server*
from the project context menu or by adding it to the instance of the Tomcat server that you
added to your Eclipse workspace when you activated the Tomcat plugin.

Start the server, go to **http://localhost:8080/test**  and bask in the glory of your (and my) labours.

Please note that the combination of Eclipse and Apache Tomcate is not very robust. I have noted 
that Eclipse sometimes spontaneously removes the Maven Dependencies from the Deployment
Assembly, resulting is a server that refuses to start without a clear error message.

Also, Apache Tomcat sometimes does not pick up changes that you make in your code. This 
happens because it makes a *copy* of your web app under its own work directory in the .metadata
part of your Eclipse workspace. You can try a combination of cleaning the Tomcat work directory
(either from the server menu or by externally removing the files from your file system, they are at
````
.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/
````

### Run Jetty Run plugin

In comparison to Apache Tomcat, it is much easier to get things up and running with this plugin.
You can find and install *Run Jetty Run* via the Eclipse Marketplace. It is a plugin that creates 
runtime configurations for the Jetty Webserver that is bundled with Eclipse (Jetty is a product
of the Eclipse Foundation).

A drawback I have noted is that sometimes it simply does not work properly and I don't know why.

When it works, these are the steps to get your webserver running:

#### 1 Install Jetty Runner in Eclipse.
Search for **jetty** in Marketplace. Take note that there is another product called *Eclipse Jetty*.
You don't want that one, you want to install **Run-Jetty-Run**. This goes pretty straight forward. You do not
need to download and install the Jetty webserver separately, it is already bundled with Eclipse.

#### 2 Rebuild
For good measure clean and build all projects. From the command line, use
````
mvn clean install
````

#### 3 Initialize a runner config
In the context menu for the *testsite* project choose *Run As->Run Jetty*. This will create a runtime configuration
but we have to tweak it a bit so stop the running Jetty server again.

#### 4 Tweak the runner config
Open up the runtime configuration (from the main menu, use *Run->Run Configurations...*) 
and edit the **Jetty Web App** for the **testsite** project. You need to change two things:

* Select a 9.4* Jetty version. The plugin starts with a default value of somewhere in version six,
and this simply does not work with modern versions of the Servlet API.
* Under *Web Application*, change the value of **Context**. The initialization in step 3
has placed the value *testsite* in this field. Change it to **test**.

Leave everything else at the default value and save the changes.

Then restart your Jetty server runner and point your browser once again to **http://localhost:8080/test**

**Note:** I have tested the description for Jetty on Windows 10 as well as linux and it works for me on both platforms.
 